export interface StaticMethods<T> {
  new(): T;
}

export function StaticImplements <T>() {
  return (_constructor: T) => { };
}