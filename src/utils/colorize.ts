import * as clc from 'cli-color';

export function typifiedString(value: any, indent: number) {
  switch (typeOf(value)) {
    case 'array':
      return arrayProperty(value, indent);
    case 'object':
      return objectProperty(value, indent);
    case 'string':
      return clc.green(value);
    case 'number':
      return clc.yellow(value);
    case 'boolean':
      return clc.yellow.bold(value);
    case 'null':
      return clc.magenta.bold('null');
    case 'undefined':
      return clc.magenta.bold('undefined');
  }
}

export function formatMeta(meta: any) {
  if (!meta || Object.keys(meta).length <= 0) {
    return '';
  }

  return objectProperty(meta);
}

function typeOf(value: any) {
  if (Array.isArray(value)) {
    return 'array';
  }

  if (!value && typeof value === 'object') {
    return 'null';
  }

  return typeof value;
}

export function formatStackTrace(stackTrace: string) {
  if (!stackTrace) {
    return '';
  }

  return clc.magenta(`\n  ${stackTrace.replace(/(\r\n|\n|\r)/gm, '$1  ')}`);
}

export function arrayProperty<T>(values: Array<T>, indent = 1) {
  if (values.length <= 0) {
    return indent === 1 ? '' : '[]';
  }

  var str = '\n';
  var prefix = getPrefix(indent);

  values.forEach((value) => {
    str += `${prefix}- ${(typifiedString(value, indent + 1) || '').trim()}\n`;
  });

  return str.substring(0, str.length - 1);
}

function objectProperty(obj: any, indent = 1) {
  if (Object.keys(obj).length === 0) {
    return indent === 1 ? '' : '{}';
  }

  var str = '\n';
  var prefix = getPrefix(indent);

  Object.keys(obj).forEach((name) => {
    var value = obj[name];
    str += `${prefix}${name}: ${typifiedString(value, indent + 1)}\n`;
  });

  return str.substring(0, str.length - 1);
}

var prefixes: any = {};
function getPrefix(indent = 1) {
  if (prefixes[indent]) {
    return prefixes[indent];
  }

  var prefix = '';
  for (let i = 0; i < indent; i++) {
    prefix += '  ';
  }

  return (prefixes[indent] = prefix);
}