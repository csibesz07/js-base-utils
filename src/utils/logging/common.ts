import { WriterProps, FileWriterProps } from './writers';

export enum LogLevel {
  error = 'error',
  warn = 'warn',
  info = 'info',
  verbose = 'verbose',
  debug = 'debug',
  silly = 'silly'
}

export const LOG_LEVELS = [LogLevel.error, LogLevel.warn, LogLevel.info, LogLevel.verbose, LogLevel.debug, LogLevel.silly];

export interface ILogger {
  log(level: LogLevel, message: string, object?: any): void;
  error(message: string, object?: any): void;
  warn(message: string, object?: any): void;
  info(message: string, object?: any): void;
  verbose(message: string, object?: any): void;
  debug(message: string, object?: any): void;
  silly(message: string, object?: any): void;
  addFileWriter(options: FileWriterProps): void;
  addConsoleWriter(options: WriterProps): void;
  removeWriter(id: string): void;
}