import { FileWriterProps, WriterProps } from './writers';
import WinstonLogger from "./WinstonLogger";
import { ILogger, LogLevel } from './common';

export default class Logger {
  static loggers = new Map<string, ILogger>();
  static isEnabled = process && process.env && process.env.NODE_ENV != 'production';
  static defaultLogger: ILogger = new WinstonLogger('default', process && process.env && process.env.NODE_ENV != 'production');

  static setDefaultLogger(logger: ILogger) {
    this.defaultLogger = logger;
  }

  static set(name: string, logger: ILogger) {
    this.loggers.set(name, logger);
  }

  static get(name: string = 'default'): ILogger {
    return (this.isEnabled && (this.loggers.get(name) || this.defaultLogger)) || new StubLogger();
  }
}


class StubLogger implements ILogger {
  _stup_promise_() {
    return new Promise<void>((resolve) => { resolve(); })
  }

  log(_level: LogLevel, _message: string): Promise<void> { return this._stup_promise_(); }
  error(_message: string): Promise<void> { return this._stup_promise_(); }
  warn(_message: string): Promise<void> { return this._stup_promise_(); }
  info(_message: string): Promise<void> { return this._stup_promise_(); }
  verbose(_message: string): Promise<void> { return this._stup_promise_(); }
  debug(_message: string): Promise<void> { return this._stup_promise_(); }
  silly(_message: string): Promise<void> { return this._stup_promise_(); }
  addFileWriter(_options: FileWriterProps): void { }
  addConsoleWriter(_options: WriterProps): void { }
  removeWriter(_id: string): void { }
}