import * as Winston from "winston"
import { ILogger, LogLevel } from './common';
import { FileWriterProps, WriterProps } from './writers';

const defaultFormatter = Winston.format.printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});


export default class WinstonLogger implements ILogger {
  logger: Winston.Logger;
  writers = new Map<String, Winston.transport>();
  level: LogLevel;
  name: string


  constructor(name: string, useConsole = true, level = LogLevel.info) {
    this.level = level;
    this.name = name;
    if (useConsole) {
      this.writers.set('default', new Winston.transports.Console());
    }
    this.logger = Winston.createLogger({
      level,
      transports: this._get_transports_(),
      format: Winston.format.combine(Winston.format.colorize(), Winston.format.label({ label: this.name }), Winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }), defaultFormatter)
    });
  }

  _get_transports_() {
    if (this.writers.size == 0) return undefined;
    let array: Winston.transport[] = [];
    this.writers.forEach((value) => {
      if (value)
        array.push(value);
    })
    return array;
  }

  _log_(level: LogLevel, message: string, object?: any): void {
    this.logger.log(level, message, object);
  }

  log(level: LogLevel, message: string, object?: any): void {
    return this._log_(level, message, object);
  }

  error(message: string, object?: any): void {
    return this._log_(LogLevel.error, message, object);
  }
  warn(message: string, object?: any): void {
    return this._log_(LogLevel.warn, message, object);
  }
  info(message: string, object?: any): void {
    return this._log_(LogLevel.info, message, object);
  }
  verbose(message: string, object?: any): void {
    return this._log_(LogLevel.verbose, message, object);
  }
  debug(message: string, object?: any): void {
    return this._log_(LogLevel.debug, message, object);
  }
  silly(message: string, object?: any): void {
    return this._log_(LogLevel.silly, message, object);
  }

  addFileWriter(options: FileWriterProps): void {
    this.writers.set(options.name, new (Winston.transports.File)(options))
    this.logger.add(this.writers.get(options.name) as Winston.transport);
  }
  addConsoleWriter(options: WriterProps): void {
    this.writers.set(options.name, new (Winston.transports.Console)(options));
    this.logger.configure({
      level: this.level,
      transports: this._get_transports_(),
    });
  }
  removeWriter(name: string): void {
    if (this.writers.has(name)) {
      this.logger.remove(this.writers.get(name) as Winston.transport);
      this.writers.delete(name);
    }
  }

}

