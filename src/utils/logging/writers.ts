import { LogLevel } from './common';

export interface WriterProps {
  level: LogLevel
  name: string
}

export interface FileWriterProps extends WriterProps {
  filename: string;
}