export default Array;

declare global {
	interface Array<T> {
		remove(elem: T): T[];
		replaceAll(search: T, replacement: T): T[];
		insert(index: number, item: T): T[];
		last(): T | null;
		contains(elem: T): boolean;
		lastValid(testFunc: (elem: T) => boolean): T | null;
		equals(b: T[]): boolean;
		permutation(): Iterable<T[]>;
		variation(k: number, filler?: boolean[]): Iterable<T[]>;
		clone(): T[];
		unique(key: (item: T) => string): T[];
	}
}

//const f = (a, b) => [].concat(...a.map(d => b.map(e => [].concat(d, e))));

Array.prototype.unique = function <T>(key: (item: T) => string): T[] {
	var seen: { [key: string]: boolean } = {};
	return this.filter(function (item: T) {
		var k = key(item);
		return seen.hasOwnProperty(k) ? false : (seen[k] = true);
	})
}

Array.prototype.permutation = function* <T>(): Iterable<T[]> {
	var permutation = this.slice();
	var length = permutation.length,
		c = Array(length).fill(0),
		i = 1, k, p;

	yield permutation.slice();
	while (i < length) {
		if (c[i] < i) {
			k = i % 2 && c[i];
			p = permutation[i];
			permutation[i] = permutation[k];
			permutation[k] = p;
			++c[i];
			i = 1;
			yield permutation.slice();
		} else {
			c[i] = 0;
			++i;
		}
	}
}

Array.prototype.variation = function* <T>(k: number, filler?: boolean[]): Iterable<T[]> {
	if (!k) {
		yield [];
		return;
	}

	if (!filler)
		filler = Array(this.length).fill(false);

	for (let i = 0; i < filler.length; ++i) {
		if (!filler[i]) {
			filler[i] = true;
			for (let variation of this.variation(k - 1, filler)) {
				yield [this[i]].concat(variation);
			}
			filler[i] = false;
		}
	}
}

Array.prototype.clone = function () {
	return this.slice(0);
};

Array.prototype.remove = function <T>(elem: T): T[] {
	return this.filter((e: T) => e !== elem);
};

Array.prototype.insert = function <T>(index: number, item: T): T[] {
	this.splice(index, 0, item);
	return this;
};

Array.prototype.last = function <T>(): T | null {
	if (this.length === 0) return null;
	return this[this.length - 1];
};

Array.prototype.lastValid = function <T>(testFunc: (elem: T) => boolean): T | null {
	let i = this.length;
	while (i--)
		if (testFunc(this[i]))
			return this[i];
	return null;
};

Array.prototype.contains = function <T>(obj: T): boolean {
	let i = this.length;
	while (i--)
		if (this[i] === obj)
			return true;
	return false;
};

Array.prototype.equals = function <T>(b: T[]): boolean {
	if (this === b) return true;
	if (this === null || b === null) return false;
	if (this.length !== b.length) return false;

	// If you don't care about the order of the elements inside
	// the array, you should sort both arrays here.

	for (let i = 0; i < this.length; ++i) {
		if (this[i] !== b[i]) return false;
	}
	return true;
};