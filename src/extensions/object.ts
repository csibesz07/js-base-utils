export default Object;

declare global {
  interface Object {
    toMap<V>(): Map<string, V>;
    fromMap<V>(map: Map<number, V>): { [key: number]: V };
    fromMap<V>(map: Map<string, V>): { [key: string]: V };
  }
}

Object.prototype.toMap = function () {
  return new Map(Object.entries(this));
}

Object.fromMap = function <V>(map: Map<number, V> | Map<string, V>) {
  let object: any = {};
  //{ [key: number]: V } | { [key: string]: V }
  for (let i of map) {
    object[i[0]] = i[1];
  }
  return object;
} 