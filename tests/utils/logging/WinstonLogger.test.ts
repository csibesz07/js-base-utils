import WinstonLogger from '../../../src/utils/logging/WinstonLogger';
import { LogLevel, LOG_LEVELS } from '../../../src/utils/logging/common';

import * as assume from 'assume'

import { expect, test } from '@jest/globals';
import * as fs from 'fs';

import * as promises from '../../../src/utils/promises';

const tmp = require('tmp');


it('Constructor_ExceptionNotThrown', () => {
  expect(() => {
    new WinstonLogger('testing');
  }).not.toThrow();
});

describe('WinstonLogger_Initialized', () => {
  let logger: WinstonLogger | undefined;
  let tmpfile: any

  beforeEach(() => {
    tmpfile = tmp.fileSync();
    logger = new WinstonLogger('testing', true, LogLevel.silly);
    logger.addFileWriter({ filename: tmpfile.name, level: LogLevel.silly, name: "FileWriter" });
  });

  afterEach(() => {
    logger = undefined;
    fs.unlinkSync(tmpfile.name);
  })

  const testMessage = "It's test message.";

  let table: LogLevel[][] = []
  for (const level of LOG_LEVELS) {
    table.push([level]);
  }

  test.each(table)("Test for each level.", async (level: LogLevel) => {
    // @ts-ignore
    logger.log(level, testMessage);

    await promises.sleep(50);

    const output = fs.readFileSync(tmpfile.name, 'utf8');

    assume(output).match(`${level}.*${testMessage}`);
  });
})