import 'jest';
import root from '../../src/utils/root';
import * as path from 'path';

it('root ok', () => {
    expect(root).toMatch(path.resolve('.'));
})