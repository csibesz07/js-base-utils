import 'jest';
import '../../src/extensions/object';

describe('object', () => {

  it('object entries', () => {
    let test = { 'test1': 1, 'test2': 2 } as Object;
    expect(Object.entries(test)).toEqual([['test1', 1], ['test2', 2]]);
  })

  it('toMap', () => {
    let test = { 'test1': 1, 'test2': 2 } as Object;
    let map = test.toMap<number>();
    expect(map.get('test1')).toBe(1);
    expect(map.get('test2')).toBe(2);
  })

  it('fromMap', () => {
    let test = new Map<string, number>();
    test.set('test1', 1);
    test.set('test2', 2);
    let obj = Object.fromMap(test);
    expect(obj['test1']).toBe(1);
    expect(obj['test2']).toBe(2);
  })
})