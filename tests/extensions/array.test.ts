import 'jest';
import '../../src/extensions/array';

describe('array', () => {

  it('variation k=3', () => {
    let all: number[][] = []

    for (let i of [1, 2, 3].variation(3)) {
      all.push(i);
    }

    expect(all[0]).toEqual([1, 2, 3]);
    expect(all[1]).toEqual([1, 3, 2]);
    expect(all.length).toBe(6);
  })

  it('variation k=2', () => {
    let all: number[][] = []

    for (let i of [1, 2, 3].variation(2)) {
      all.push(i);
    }

    expect(all[0]).toEqual([1, 2]);
    expect(all[1]).toEqual([1, 3]);
    expect(all.length).toBe(6);
  })

  it('permutation', () => {
    let all: number[][] = []

    for (let i of [1, 2, 3].permutation()) {
      all.push(i);
    }

    expect(all[0]).toEqual([1, 2, 3]);
    expect(all[1]).toEqual([2, 1, 3]);
    expect(all.length).toBe(6);
  })

  it('unique_basic_json', () => {
    let array = [[1, 2, 3], [4, 5, 6], [1, 2, 3]];
    expect(array.unique(JSON.stringify)).toMatchObject([[1, 2, 3], [4, 5, 6]]);
  })

  it('unique_complex_json', () => {
    let array = [{ id: 1 }, { id: 2 }, { id: 1 }];
    expect(array.unique((item) => item.id.toLocaleString())).toMatchObject([{ id: 1 }, { id: 2 }]);
  })
})